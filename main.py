from os.path import exists
import torch
import torch.nn as nn
from torch.nn.functional import log_softmax, pad
import math
import copy
import time
from torch.optim.lr_scheduler import LambdaLR
import pandas as pd
from torchtext.data.functional import to_map_style_dataset
from torch.utils.data import DataLoader
from torchtext.vocab import build_vocab_from_iterator
import torchtext.datasets as datasets
# import spacy
# import GPUtil
import warnings
from torch.utils.data.distributed import DistributedSampler
import torch.distributed as dist
import torch.multiprocessing as mp
import unittest
from torch.nn.parallel import DistributedDataParallel as DDP


# Set to False to skip notebook execution (e.g. for debugging)
warnings.filterwarnings("ignore")
RUN_EXAMPLES = True

# Some convenience helper functions used throughout the notebook


def is_interactive_notebook():
    return __name__ == "__main__"


def show_example(fn, args=[]):
    if __name__ == "__main__" and RUN_EXAMPLES:
        return fn(*args)


def execute_example(fn, args=[]):
    if __name__ == "__main__" and RUN_EXAMPLES:
        fn(*args)
        
from tf.log import Log, FLog
from tf.decoder import Decoder
from tf.encoderdecoder import EncoderDecoder
from tf.encoder import Encoder
from tf.encoderlayer import EncoderLayer
from tf.utils import clones, subsequent_mask, greedy_decode
from tf.multiheadedattention import MultiHeadedAttention
from tf.multiheadedattention import attention
from tf.positionwisefeedforward import PositionwiseFeedForward
from tf.layernorm import LayerNorm
from tf.sublayerconnection import SublayerConnection
from tf.positionalencoding import PositionalEncoding
from tf.decoderlayer import DecoderLayer
from tf.batch import Batch
from tf.simplelosscompute import SimpleLossCompute
from tf.embeddings import Embeddings
from tf.generator import Generator
from tf.makemodel import make_model
from tf.trainstate import TrainState
from tf.runepoch import run_epoch
from tf.scheduler import rate
from tf.labelsmoothing import LabelSmoothing
from tf.loss import loss
from tf.simplemodel import data_gen, example_simple_model
from tf.test.test import runner

execute_example(example_simple_model)
