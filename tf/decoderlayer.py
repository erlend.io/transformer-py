from tf.sublayerconnection import SublayerConnection
from tf.utils import clones
import torch.nn as nn

class DecoderLayer(nn.Module):
    "Decoder is made of self-attn, src-attn, and feed forward (defined below)"

    def __init__(self, size, self_attn, src_attn, feed_forward, dropout):
        super(DecoderLayer, self).__init__()
        self.size = size
        self.self_attn = self_attn
        self.src_attn = src_attn
        self.feed_forward = feed_forward
        self.sublayer = clones(SublayerConnection(size, dropout), 3)
        
    def lamb(self, x, tgt_mask):
        return self.self_attn(x, x, x, tgt_mask)
        

    def forward(self, x, memory, src_mask, tgt_mask, log=None):
        "Follow Figure 1 (right) for connections."
        
        m = memory
        x = self.sublayer[0](x, lambda x: self.lamb(x, tgt_mask))                             
        x = self.sublayer[1](x, lambda x: self.src_attn(x, m, m, src_mask))
        x = self.sublayer[2](x, self.feed_forward)
        return x
 