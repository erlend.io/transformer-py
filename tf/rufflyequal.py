import torch

def ruffly_equal(v1, v2) :
    if isinstance(v1, float) and isinstance(v2, float):
        return abs(v1 - v2) < 0.001
    
    if not torch.is_tensor(v1) or not torch.is_tensor(v2):
        return False
    
    if v1.shape != v2.shape:
        return False
    
    if v1.dtype != v2.dtype:
        return False
    
    if v1.dtype != torch.float32 :
        return False
    
    f1 = v1.flatten();
    f2 = v2.flatten();
    
    assert len(f1) == len(f2), "Length should be equal"
    
    for i in range(len(f1)):
        fl1 = f1[i].item();
        fl2 = f2[i].item();
        assert isinstance(fl1, float) and isinstance(fl2, float), "Tensors should contain floats"
        if not ruffly_equal(fl1, fl2):
            print(fl1)
            print(fl2)
            return False

    return True
