import torch
import torch.nn as nn
from torch.optim.lr_scheduler import LambdaLR
from tf.batch import Batch
from tf.labelsmoothing import LabelSmoothing
from tf.log import FLog
from tf.makemodel import make_model
from tf.scheduler import rate
from tf.utils import greedy_decode
from tf.runepoch import run_epoch
from tf.simplelosscompute import SimpleLossCompute
from tf.utils import DummyOptimizer

def data_gen(V, batch_size, nbatches):
    "Generate random data for a src-tgt copy task."
    dataset = []    
    
    for i in range(nbatches):
        data = torch.randint(1, V, size=(batch_size, 10))
        data[:, 0] = 1
        src = data.requires_grad_(False).clone().detach()
        tgt = data.requires_grad_(False).clone().detach()
        dataset.append(Batch(src, tgt, 0))
    return dataset

def example_simple_model():
    V = 11
    criterion = LabelSmoothing(size=V, padding_idx=0, smoothing=0.0)
    
    log = FLog("/home/erlend/Projects/transformer/log/python.log")    
    torch.manual_seed(27)
    model = make_model(V, V, N=2, seed=27, dropout=0, log=log)
    log.print_model_parameters("EncoderDecoder", model)   

    optimizer = torch.optim.Adam(
        model.parameters(), lr=0.5, betas=(0.9, 0.98), eps=1e-9
    )
    lr_scheduler = LambdaLR(
        optimizer=optimizer,
        lr_lambda=lambda step: rate(
            step, model_size=model.src_embed[0].d_model, factor=1.0, warmup=400
        ),
    )
    
    batch_size = 80
    torch.manual_seed(47)
    train_data_set = data_gen(V, batch_size, 20)
    
    torch.manual_seed(1243)
    eval_data_set = data_gen(V, batch_size, 5)
        
    for epoch in range(20):
        model.train()        
        run_epoch(
            train_data_set,      
            model,
            SimpleLossCompute(model.generator, criterion),
            optimizer,
            lr_scheduler,
            mode="train",
            log=log
        )
        model.eval()
        torch.manual_seed(1243+epoch)
        run_epoch(
            eval_data_set,
            model,
            SimpleLossCompute(model.generator, criterion),
            DummyOptimizer(),
            lr_scheduler,
            mode="eval",
            log=log
        )[0]

    model.eval()
    src = torch.LongTensor([[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]])
    max_len = src.shape[1]
    src_mask = torch.ones(1, 1, max_len)
    print(greedy_decode(model, src, src_mask, max_len=max_len, start_symbol=0))
