import copy
import torch
import torch.nn as nn
from tf.decoder                 import Decoder
from tf.decoderlayer            import DecoderLayer
from tf.embeddings              import Embeddings
from tf.encoder                 import Encoder
from tf.encoderdecoder          import EncoderDecoder
from tf.encoderlayer            import EncoderLayer
from tf.generator               import Generator
from tf.multiheadedattention    import MultiHeadedAttention
from tf.positionalencoding      import PositionalEncoding
from tf.positionwisefeedforward import PositionwiseFeedForward

def make_model(src_vocab, tgt_vocab, N=6, d_model=512, d_ff=2048, h=8, dropout=0.1, seed=0, log=None):
    "Helper: Construct a model from hyperparameters."
    c        = copy.deepcopy    
    attn     = MultiHeadedAttention(h, d_model, dropout)    
    torch.manual_seed(seed*41+17)       # Has to be the inorder to make it repeatable    
    ff       = PositionwiseFeedForward(d_model, d_ff, dropout)
    position = PositionalEncoding(d_model, dropout)    
    
    # encoder
    encoder_layer = EncoderLayer(d_model, c(attn), c(ff), dropout)
    encoder       = Encoder(encoder_layer, N)
    
    # decoder
    decoder_layer = DecoderLayer(d_model, c(attn), c(attn), c(ff), dropout)
    decoder       = Decoder(decoder_layer, N)
    
    # src embedding
    # Has to be the inorder to make it repeatable    
    torch.manual_seed(seed*21+15) 
    src_embedding = Embeddings(d_model, src_vocab)
    srcSeq        = nn.Sequential(src_embedding, c(position))
    
    # tgt embedding
    # Has to set seed inorder to make it repeatable
    torch.manual_seed(seed*17*11)    
    tgtSeq = nn.Sequential(Embeddings(d_model, tgt_vocab), c(position))
    
    # Generator
    gen = Generator(d_model, tgt_vocab)
    
    model = EncoderDecoder(
        encoder,
        decoder,
        srcSeq,
        tgtSeq,
        gen
    )
           
    # This was important from their code.
    # Initialize parameters with Glorot / fan_avg.
    for p in model.parameters():
        if p.dim() > 1:
            nn.init.xavier_uniform_(p)
    return model
