import torch.nn as nn
from tf.layernorm import LayerNorm

class SublayerConnection(nn.Module):
    """
    A residual connection followed by a layer norm.
    Note for code simplicity the norm is first as opposed to last.
    """

    def __init__(self, size, dropout):
        super(SublayerConnection, self).__init__()
        self.norm    = LayerNorm(size)
        self.dropout = nn.Dropout(dropout)
        self.log     = None

    def forward(self, x, sublayer):
        "Apply residual connection to any sublayer with the same size."
        if self.log :            
            self.log.enter("SublayerConnectionImpl::forward")
            self.log.print_tensor("x", x)

            norm_x = self.norm(x)
            self.log.print_tensor("norm-x", norm_x)
            
            sub = sublayer(norm_x)
            self.log.print_tensor("sublayer", sub)

            y = x + self.dropout(sub)
            self.log.print_tensor("y", y)
            self.log.exit()
            return y;       
        else :
            return x + self.dropout(sublayer(self.norm(x)))

    def set_log(self, log) :
        self.log = log
        