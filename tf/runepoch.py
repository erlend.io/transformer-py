import time
import torch
from tf.trainstate import TrainState

def run_epoch(
    data_iter,
    model,
    loss_compute,
    optimizer,
    scheduler,
    mode="train",
    accum_iter=1,
    train_state=TrainState(),
    log=None
):
    """Train a single epoch"""
    start = time.time()
    total_tokens = 0
    total_loss = 0
    tokens = 0
    n_accum = 0
    
    log.print_int("stepCount", scheduler._step_count)
    
    for i, batch in enumerate(data_iter):
        torch.manual_seed(4712+i)
        out = model.forward(
            batch.src, batch.tgt, batch.src_mask, batch.tgt_mask
        )
        loss, loss_node = loss_compute(out, batch.tgt_y, batch.ntokens)

        if log :
            log.print_tensor("out", out)
            log.print_tensor("loss", loss)
            log.print_tensor("loss-node", loss_node)

        # loss_node = loss_node / accum_iter
        if mode == "train" or mode == "train+log":
            loss_node.backward()
            train_state.step += 1
            train_state.samples += batch.src.shape[0]
            train_state.tokens += batch.ntokens
            if i % accum_iter == 0:
                optimizer.step()
                optimizer.zero_grad(set_to_none=True)
                n_accum += 1
                train_state.accum_step += 1
                if log :
                    log.print_float("lr", optimizer.param_groups[0]["lr"])
                    log.print_model_parameters("model-after", model)
            scheduler.step()

        total_loss += loss
        total_tokens += batch.ntokens
        tokens += batch.ntokens
        if i % 40 == 1 and (mode == "train" or mode == "train+log"):
            if log:
                log.print_tensor("out", out)
                log.print_tensor("batch", batch.tgt_y)
                log.print_int("ntokens", batch.ntokens)
                log.print_int("stepCount", scheduler._step_count)            
                lr = optimizer.param_groups[0]["lr"]
                log.print_int("lr", lr)
            
            elapsed = time.time() - start
            print(
                (
                    "Epoch Step: %6d | Accumulation Step: %3d | Loss: %6.2f "
                    + "| Tokens / Sec: %7.1f | Learning Rate: %6.1e"
                )
                % (i, n_accum, loss / batch.ntokens, tokens / elapsed, lr)
            )
            start = time.time()
            tokens = 0
        del loss
        del loss_node
    return total_loss / total_tokens, train_state