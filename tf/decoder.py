from tf.utils import clones
from tf.layernorm import LayerNorm
import torch.nn as nn

class Decoder(nn.Module):
    "Generic N layer decoder with masking."

    def __init__(self, layer, N):
        super(Decoder, self).__init__()
        self.layers = clones(layer, N)
        self.norm = LayerNorm(layer.size)
        self.log  = None

    def forward(self, x, memory, src_mask, tgt_mask):
        i = 0        

        if self.log:
            self.log.enter("DecoderImpl::forward")
            self.log.print_tensor("x", x)            

        for layer in self.layers:
            x = layer(x, memory, src_mask, tgt_mask)
            if self.log :
                i = i + 1
                self.log.print_tensor("X"+str(i), x)

        normalized = self.norm(x)

        if self.log:
            self.log.print_tensor("normalized", normalized)
            self.log.exit()            

        return normalized

    def set_log(self, log) :
        self.log = log

