import torch.nn as nn

class EncoderDecoder(nn.Module):
    """
    A standard Encoder-Decoder architecture. Base for this and many
    other models.
    """    

    def __init__(self, encoder, decoder, src_embed, tgt_embed, generator):
        super(EncoderDecoder, self).__init__()
        self.encoder   = encoder
        self.decoder   = decoder
        self.src_embed = src_embed
        self.tgt_embed = tgt_embed
        self.generator = generator
        self.log       = None

    def forward(self, src, tgt, src_mask, tgt_mask):
        "Take in and process masked src and target sequences."
        if self.log:
            self.log.enter("EncoderDecoderImpl::forward")
            self.log.print_tensor("src", src);
            self.log.print_tensor("src_mask", src_mask);

            encoded = self.encode(src, src_mask)
            self.log.print_tensor("encoded", encoded)

            y = self.decode(encoded, src_mask, tgt, tgt_mask)
            self.log.exit()
            return y                        
        else:
            return self.decode(self.encode(src, src_mask), src_mask, tgt, tgt_mask)

    def encode(self, src, src_mask):
        if self.log :
            src_embedding = self.src_embed(src);
            self.log.print_tensor("src-embedding", src_embedding);
            return self.encoder(src_embedding, src_mask)
        else :
            return self.encoder(self.src_embed(src), src_mask)

    def decode(self, memory, src_mask, tgt, tgt_mask):
        if self.log :
            embedding = self.tgt_embed(tgt); 
            self.log.print_tensor("embedding", embedding);

            return self.decoder(embedding, memory, src_mask, tgt_mask);    
        else:
            return self.decoder(self.tgt_embed(tgt), memory, src_mask, tgt_mask)

    def setLog(self, log):
        self.log = log
