import torch.nn as nn
from tf.utils import clones
from tf.layernorm import LayerNorm

class Encoder(nn.Module):
    "Core encoder is a stack of N layers"
 
    def __init__(self, layer, N):
        super(Encoder, self).__init__()
        self.layers = clones(layer, N)
        self.norm = LayerNorm(layer.size)
        self.log  = None

    def forward(self, x, mask):
        "Pass the input (and mask) through each layer in turn."
        i = 0        

        if self.log:
            self.log.enter("EncoderImpl::forward")
            self.log.print_tensor("x", x)            

        for layer in self.layers:
            x = layer(x, mask)
            if self.log :
                i = i + 1
                self.log.print_tensor("X"+str(i), x)

        normalized = self.norm(x)
        if self.log:
            self.log.print_tensor("normalized", normalized)
            self.log.exit()            

        return normalized

    def setLog(self, log) :
        self.log = log
