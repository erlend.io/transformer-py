from tf.sublayerconnection import SublayerConnection
from tf.utils import clones
import copy
import torch.nn as nn

class EncoderLayer(nn.Module):
    "Encoder is made up of self-attn and feed forward (defined below)"

    def __init__(self, size, self_attn, feed_forward, dropout):
        super(EncoderLayer, self).__init__()
        self.self_attn = self_attn
        self.feed_forward = feed_forward
        subcon = SublayerConnection(size, dropout)
        self.sublayer1 = copy.deepcopy(subcon)
        self.sublayer2 = copy.deepcopy(subcon)
        self.sublayer = nn.ModuleList([self.sublayer1, self.sublayer2])
        self.size = size
        self.log  = None        

    def forward(self, x, mask):
        "Follow Figure 1 (left) for connections."
        if self.log :
            self.log.enter("EncoderLayerImpl::forward")
            self.log.print_tensor("x", x)

        layer1 = self.sublayer1(x, lambda x_: self.self_attn(x_, x_, x_, mask))
        layer2 = self.sublayer2(layer1, self.feed_forward)

        if self.log :
            self.log.print_tensor("Layer1", layer1)
            self.log.print_tensor("Layer2", layer2)
            self.log.exit()            

        return layer2        

    def set_log(self, log):
        self.log = log

        