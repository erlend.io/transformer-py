import torch.nn as nn
import torch
import math
from tf.utils import clones

def attention(query, key, value, mask=None, dropout=None, log=None):
    "Compute 'Scaled Dot Product Attention'"
    # Size of the vector is:
    # [Batch, Word, d_model]        
    if log is not None:
        log.print_tensor("query", query)
        log.print_tensor("key",   key)
        log.print_tensor("value", value)
    
    d_k = query.size(-1)
    scores = torch.matmul(query, key.transpose(-2, -1)) / math.sqrt(d_k)
    if mask is not None:
        scores = scores.masked_fill(mask == 0, -1e9)
                
    p_attn = scores.softmax(dim=-1)
    
    if dropout is not None:
        if log is not None:        
            log.print_tensor("attn(pre-dropout)", p_attn)
        p_attn = dropout(p_attn)

    y = torch.matmul(p_attn, value)
    if log is not None:        
        log.print_tensor("scores", scores)
        log.print_tensor("attn",   p_attn)
        log.print_tensor("y",      y)
                
    return y, p_attn

class MultiHeadedAttention(nn.Module):
    def __init__(self, h, d_model, dropout=0.1):
        "Take in model size and number of heads."
        super(MultiHeadedAttention, self).__init__()
        assert d_model % h == 0
        # We assume d_v always equals d_k
        self.d_k = d_model // h
        self.h = h
        self.linears = clones(nn.Linear(d_model, d_model), 4)
        self.attn = None
        self.dropout = nn.Dropout(p=dropout)
        self.log = None        
        
    def test(self, lin, x):
        print("Lin", x)
        return lin(x)    

    def forward(self, query_in, key_in, value_in, mask=None):
        "Implements Figure 2"
        "query_in [batch,word,d_model]"
        "key_in   [batch,word,d_model]"        
        if mask is not None:
            # Same mask applied to all h heads.
            mask = mask.unsqueeze(1)
        nbatches = query_in.size(0)
        
        # print(nbatches)        
        
        # for lin, x in zip(self.linears, (query, key, value)) :
        #    print("Est:", x)       
        #    print("Line:", lin.weight)           

        # 1) Do all the linear projections in batch from d_model => h x d_k
        query = self.linears[0](query_in).view(nbatches, -1, self.h, self.d_k).transpose(1, 2)
        key   = self.linears[1](key_in).view(nbatches, -1, self.h, self.d_k).transpose(1, 2)
        value = self.linears[2](value_in).view(nbatches, -1, self.h, self.d_k).transpose(1, 2)
               
        # 2) Apply attention on all the projected vectors in batch.
        x, self.attn = attention(query, key, value, mask=mask, dropout=self.dropout, log=self.log)
        
        # 3) "Concat" using a view and apply a final linear.
        x = (
            x.transpose(1, 2)
            .contiguous()
            .view(nbatches, -1, self.h * self.d_k)
        )
        
        del query
        del key
        del value
        return self.linears[-1](x)
    
    def set_log(self, log):
        self.log = log
        