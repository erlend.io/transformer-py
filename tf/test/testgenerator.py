import unittest
import torch
from tf.generator import Generator

class TestGenerator(unittest.TestCase):
    def setUp(self):
        torch.manual_seed(0)
        self.gen = Generator(2, 5)
        
    def test_weights(self):
        target_weight = torch.tensor([[-0.0053,  0.3793], [-0.5820, -0.5204], [-0.2723,  0.1896], [-0.0140,  0.5607], [-0.0628,  0.1871]], dtype=torch.float32)
        self.assertTrue(torch.allclose(target_weight, self.gen.proj.weight, 1e-4, 1e-4))

    def test_bias(self):
        target_bias   = torch.tensor([-0.2137, -0.1390, -0.6755, -0.4683, -0.2915], dtype=torch.float32)
        self.assertTrue(torch.allclose(target_bias, self.gen.proj.bias, 1e-4, 1e-4))        

    def test_result(self):
        x             = torch.tensor([1.0, 2.0])
        y             = self.gen.forward(x)
        target_result = torch.tensor([-1.1408, -3.4422, -2.2491, -1.0415, -1.6605], dtype=torch.float32)
        
        self.assertTrue(torch.allclose(y, target_result, 1e-4, 1e-4))
        
        
def generator_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestGenerator('test_weights'))
    suite.addTest(TestGenerator('test_bias'))
    suite.addTest(TestGenerator('test_result'))
    return suite
