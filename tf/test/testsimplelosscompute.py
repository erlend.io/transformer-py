import unittest
import torch
from tf.generator import Generator
from tf.labelsmoothing import LabelSmoothing
from tf.simplelosscompute import SimpleLossCompute

class TestSimpleLossCompute(unittest.TestCase):
        
    def test(self):
        d_model     = 5
        vocab       = 10
        padding     = 2
        token_count = 10
        
        torch.manual_seed(10)
        gen         = Generator(d_model, vocab)        
        criterion   = LabelSmoothing(vocab, padding, 0.05);
        loss        = SimpleLossCompute(gen, criterion);
        token_count = torch.tensor(token_count)
        
        torch.manual_seed(2)
        x      = torch.randn(token_count, d_model)
        target = torch.randint(0, vocab, (1, token_count))        
        y, l   = loss(x, target, token_count)
                
        self.assertTrue(torch.allclose(y, torch.tensor(20.1243)))
        self.assertTrue(torch.allclose(l, torch.tensor(2.0124), 1e-4, 1e-4))
            
def simple_loss_compute():
    suite = unittest.TestSuite()
    suite.addTest(TestSimpleLossCompute('test'))
    return suite
        