import torch
import unittest
from tf.scheduler import rate
from torch.optim.lr_scheduler import LambdaLR

class TestScheduler(unittest.TestCase):
        
    def test(self):
        dummy_model = torch.nn.Linear(1, 1)
        learning_rates = []
                
        # run 20000 epoch for each example
        optimizer = torch.optim.Adam(
            dummy_model.parameters(), lr=1, betas=(0.9, 0.98), eps=1e-9
        )
        lr_scheduler = LambdaLR(
            optimizer=optimizer, lr_lambda=lambda step: rate(step, 512, 1, 4000)
        )
        tmp = []
        # take 20K dummy training steps, save the learning rate at each step
        for step in range(20000):
            tmp.append(optimizer.param_groups[0]["lr"])
            optimizer.step()
            lr_scheduler.step()
                    
        self.assertTrue(abs(tmp[2000] - 0.0003493856214843422) < 1e-8)
        self.assertTrue(abs(tmp[4000] - 0.0006987712429686843) < 1e-8)
        self.assertTrue(abs(tmp[8000] - 0.0004941058844013093) < 1e-8)
        
        
            
def scheduler_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestScheduler('test'))
    return suite
