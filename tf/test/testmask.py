import torch
import unittest
from tf.utils import subsequent_mask


class TestMask(unittest.TestCase):
        
    def test_mask(self):
        mask   = subsequent_mask(3)
        target = torch.tensor([[[True, False, False], [True, True, False], [True, True, True]]])
                
        self.assertTrue(torch.allclose(mask, target, 1e-3, 1e-4))
               
def tast_mask_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestMask('test_mask'))
    return suite
        