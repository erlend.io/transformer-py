import copy
import torch
import torch.nn as nn
import unittest
from tf.decoderlayer import DecoderLayer
from tf.multiheadedattention import MultiHeadedAttention
from tf.positionalencoding import PositionalEncoding
from tf.positionwisefeedforward import PositionwiseFeedForward
from tf.utils import subsequent_mask

class TestDecoderLayer(unittest.TestCase):

    def setUp(self):
        h         = 2      # number of heads                
        d_model   = 4      # number of features                
        dropout   = 0.1    # Dropout persentage
        d_ff      = 8      # Inner dimension of FeedForward network        
        
        torch.manual_seed(0)
        self.self_attn = MultiHeadedAttention(h, d_model, dropout)
        attn           = copy.deepcopy(self.self_attn)
        torch.manual_seed(0);
        self.ff    = PositionwiseFeedForward(d_model, d_ff, dropout)
        position   = PositionalEncoding(d_model, dropout)
        self.model = DecoderLayer(d_model, self.self_attn, attn, self.ff, dropout)
        
    def test_forward(self):
        # Dimension [Batch, Word, DWord]
        x    = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mem  = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask = subsequent_mask(3)
        
        # Run model  
        log = None
        if log is not None:
            log.print_tensor("x", x)
            log.print_tensor("mem", mem)
            log.print_model_parameters("PositionWiseFeedForward", self.ff)            
                    
        torch.manual_seed(0)
        y      = self.model.forward(x, mem, mask, mask, log)
        
        if log is not None:            
            log.print_tensor("y", y)        
            
        target = torch.tensor([[[ 1.5030,  2.8105,  3.5600,  4.6145], [ 7.6768,  5.8654,  7.0365,  7.9868], [11.2782,  9.2273, 10.4157, 11.3932]]], dtype=torch.float32)
        self.assertTrue(torch.allclose(target, y, 1e-3, 1e-4))
        
    def test_learning(self):
        torch.manual_seed(0)        
        log = None
        # log.print_model_parameters("DecoderLayer", self.model)
        
        # setup optimizer        
        optimizer = torch.optim.SGD(self.model.parameters(), lr=0.00001)
        loss      = nn.MSELoss()
        
        # setup test example
        x      = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mem    = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask   = subsequent_mask(3)
        target = torch.tensor([[[ 4.0,  2.0,  3, 5], [ 10,  7.0, 7.0,  8.0], [12.0, 9.0, 10.0, 10]]], dtype=torch.float32)
        
        target_before = torch.tensor([[[ 1.5030,  2.8105,  3.5600,  4.6145],[ 7.6768,  5.8654,  7.0365,  7.9868],[11.2782,  9.2273, 10.4157, 11.3932]]], dtype=torch.float32)
        target_after  = torch.tensor([[[ 3.9949,  2.9693,  3.7775,  4.4186],[ 7.7347,  6.2257,  6.6365,  7.2300],[11.4679,  9.2271, 10.4157, 11.3928]]], dtype=torch.float32)
        
        # run model
        y = self.model.forward(x, mem, mask, mask)
        if log is not None:
            log.print_tensor("y", y)
            
        self.assertTrue(torch.allclose(y, target_before, 1e-3, 1e-4))
        
        # train        
        l = loss(y, target)
        
        l.backward()        
        optimizer.step();
        optimizer.zero_grad()
        
        # test result of training        
        y = self.model.forward(x, mem, mask, mask)
        if log is not None:
            log.print_tensor("y", y)
        
        l = loss(target, y)
        self.assertTrue(torch.allclose(y, target_after, 1e-3, 1e-4))
        
                       
def test_decoder_layer():
    suite = unittest.TestSuite()
    suite.addTest(TestDecoderLayer('test_forward'))
    suite.addTest(TestDecoderLayer('test_learning'))
    return suite
