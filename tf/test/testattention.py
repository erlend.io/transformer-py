import torch
import unittest
from tf.log import Log
from tf.multiheadedattention import attention
from tf.utils import subsequent_mask

class TestAttention(unittest.TestCase):
        
    def test_attention(self):
        # Size of the vector is:
        # [Batch, Word, d_model]        
        
        query  = torch.tensor([[[1.0, 2.0], [3.0, 4.0]]], dtype=torch.float32)
        key    = torch.tensor([[[3.0, 4.0], [5.0, 6.0]]], dtype=torch.float32)
        value  = torch.tensor([[[5.0, 7.0], [8.0, 9.0]]], dtype=torch.float32)
        
        y, attn = attention(query, key, value, None, None, None)

        target_y    = torch.tensor([[[7.9575, 8.9717], [7.9998, 8.9999]]], dtype=torch.float32)
        target_attn = torch.tensor([[[1.4166e-02, 9.8583e-01], [5.0197e-05, 9.9995e-01]]], dtype=torch.float32)        
        self.assertTrue(torch.allclose(y, target_y))
        self.assertTrue(torch.allclose(attn, target_attn))
        
    def test_filter(self) :
        # Size of the vector is:
        # [Batch, Word, d_model]        
        
        query  = torch.tensor([[[1.0, 2.0], [3.0, 4.0], [3.5, 4.5]]], dtype=torch.float32)
        key    = torch.tensor([[[3.0, 4.0], [5.0, 6.0], [-1,  2.0]]], dtype=torch.float32)
        value  = torch.tensor([[[5.0, 7.0], [8.0, 9.0], [4,   5.0]]], dtype=torch.float32)
        mask   = subsequent_mask(3)   
        mask   = mask.unsqueeze(1)

        y, attn = attention(query, key, value, mask)

        target_y    = torch.tensor([[[5.0000, 7.0000], [7.9998, 8.9999], [8.0000, 9.0000]]], dtype=torch.float32)
        target_attn = torch.tensor([[[1.0000e+00, 0.0000e+00, 0.0000e+00],[5.0197e-05, 9.9995e-01, 0.0000e+00],[1.2204e-05, 9.9999e-01, 1.0553e-12]]], dtype=torch.float32)        
        self.assertTrue(torch.allclose(y, target_y))
        self.assertTrue(torch.allclose(attn, target_attn))
                       
def attention_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestAttention('test_attention'))
    suite.addTest(TestAttention('test_filter'))
    return suite
        