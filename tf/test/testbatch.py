import unittest
import torch

from tf.batch import Batch


class TestBatch(unittest.TestCase):
        
    def test(self):
        torch.manual_seed(2)
        src = torch.randint(0, 3, (3, 4), dtype=torch.int)
        tgt = torch.randint(0, 3, (3, 4), dtype=torch.int)
        batch = Batch(src, tgt, 2)
        
        # src
        target_src = torch.tensor([[0, 0, 2, 0], [2, 0, 0, 1], [2, 0, 0, 1]], dtype=torch.int32)        
        self.assertTrue(torch.allclose(batch.src, target_src))
        
        # src mask
        target_src_mask = torch.BoolTensor([[[ True,  True, False,  True]], [[False,  True,  True,  True]], [[False,  True,  True,  True]]])
        self.assertTrue(torch.allclose(batch.src_mask, target_src_mask))
        
        # tgt
        target_tgt = torch.tensor([[2, 1, 0], [1, 2, 2],[1, 0, 1]], dtype=torch.int32)
        self.assertTrue(torch.allclose(batch.tgt, target_tgt))
        
        # tgt_y
        target_tgt_y = torch.tensor([[1, 0, 2], [2, 2, 0], [0, 1, 2]], dtype=torch.int32)
        self.assertTrue(torch.allclose(batch.tgt_y, target_tgt_y))
        
        # tgt_mask 
        target_tgt_mask = torch.BoolTensor([[[False, False, False], [False,  True, False], [False,  True,  True]], [[ True, False, False], [ True, False, False], [ True, False, False]], [[ True, False, False], [ True,  True, False],[ True,  True,  True]]])
        self.assertTrue(torch.allclose(batch.tgt_mask, target_tgt_mask))

        self.assertTrue(batch.ntokens.item() == 5)
               
def batch_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestBatch('test'))
    return suite
