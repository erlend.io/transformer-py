import torch
import torch.nn as nn
import unittest
from tf.encoder import Encoder
from tf.encoderlayer import EncoderLayer
from tf.multiheadedattention import MultiHeadedAttention
from tf.positionwisefeedforward import PositionwiseFeedForward
from tf.utils import subsequent_mask

class TestEncoder(unittest.TestCase):

    def setUp(self):
        h         = 2      # number of heads                
        d_model   = 4      # number of features                
        dropout   = 0.1    # Dropout persentage
        d_ff      = 8      # Inner dimension of FeedForward network        
        
        torch.manual_seed(0)
        self.attn = MultiHeadedAttention(h, d_model, dropout)
        torch.manual_seed(0);
        self.ff    = PositionwiseFeedForward(d_model, d_ff, dropout)
        layer      = EncoderLayer(d_model, self.attn, self.ff, dropout)
        self.model = Encoder(layer, 5)
        
    def test_forward(self):
        # Dimension [Batch, Word, DWord]
        target_before = torch.tensor([[[-1.0422,  0.5514, -0.6175,  1.1083],[-1.1034,  1.2071, -0.4518,  0.3481],[-0.9205,  1.4177, -0.1502, -0.3469]]], dtype=torch.float32)
        
        x    = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask = subsequent_mask(3)
        
        # Run model  
        torch.manual_seed(0)
        y = self.model.forward(x, mask)
        
        self.assertTrue(torch.allclose(target_before, y, 1e-3, 1e-4))
        
    def test_learning(self):
        torch.manual_seed(0)        

        # setup optimizer        
        optimizer = torch.optim.SGD(self.model.parameters(), lr=0.00001)
        loss      = nn.MSELoss()
        
        # setup test example
        x      = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask   = subsequent_mask(3)
        target = torch.tensor([[[4.0, 2.0, 3.0, 5.0], [10.0,7.0, 7.0, 8.0], [12.0, 9.0, 10.0, 10.0]]], dtype=torch.float32)
        
        target_before = torch.tensor([[[-1.0422,  0.5514, -0.6175,  1.1083],[-1.1034,  1.2071, -0.4518,  0.3481],[-0.9205,  1.4177, -0.1502, -0.3469]]], dtype=torch.float32)
        target_after  = torch.tensor([[[-1.0431,  0.9264, -0.6650,  0.7819],[-1.1326,  1.1267, -0.4701,  0.4761],[-0.6921,  1.4842, -0.4437, -0.3482]]], dtype=torch.float32)
        
        # run model
        y = self.model.forward(x, mask)
#        self.assertTrue(torch.allclose(y, target_before, 1e-3, 1e-4))
        
        # train        
        l = loss(y, target)       
        l.backward()        
        optimizer.step();
        optimizer.zero_grad()
        
        # test result of training        
        y = self.model.forward(x, mask)
                
        l = loss(target, y)
        self.assertTrue(torch.allclose(y, target_after, 1e-3, 1e-4))        
                       
def test_encoder():
    suite = unittest.TestSuite()
    suite.addTest(TestEncoder('test_forward'))
    suite.addTest(TestEncoder('test_learning'))
    return suite
