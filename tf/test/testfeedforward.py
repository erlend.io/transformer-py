import unittest
import torch
import torch.nn as nn
from tf.positionwisefeedforward import PositionwiseFeedForward


class TestFeedForward(unittest.TestCase):
        
    def test_forward(self):        
        torch.manual_seed(0)
        model     = PositionwiseFeedForward(4, 8);        
        optimizer = torch.optim.SGD(model.parameters(), lr=0.1)
        loss      = nn.MSELoss()
                
        query  = torch.tensor([[[1, 2, 3, 4], [5,6,7,8]]], dtype=torch.float32)
        torch.manual_seed(0)
        y      = model.forward(query)       
        
        # Test answer        
        target = torch.tensor([[[-0.4875,  0.2235,  0.3933,  1.1167], [-1.6433,  0.5783,  0.4452,  2.4791]]], dtype=torch.float32)        
        self.assertTrue(torch.allclose(y, target, 1e-3, 1e-3))        
        
        # Try leading
        learning_target = torch.tensor([[[-0.0,  0.5,  0.5,  1.0], [-2.0,  1.0,  1.0,  3]]], dtype=torch.float32)                
        l = loss(learning_target, y)
        l.backward()
        optimizer.step()
        optimizer.zero_grad()

        y = model.forward(query)
        
        target = torch.tensor([[[-0.6918, 0.7226, 0.9900, 1.7287], [-2.0019, 1.5458, 1.8874, 3.8237]]], dtype=torch.float32)
        self.assertTrue(torch.allclose(y, target, 1e-3, 1e-3))
                      
def test_feedforward():
    suite = unittest.TestSuite()
    suite.addTest(TestFeedForward('test_forward'))    
    return suite
        
