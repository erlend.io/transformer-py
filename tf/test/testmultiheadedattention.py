from tf.multiheadedattention import MultiHeadedAttention
from tf.utils import subsequent_mask
import copy
import torch
import torch.nn as nn
import unittest

class TestMultiHeadedAttention(unittest.TestCase):
    
    def test_model(self, model, target_before, target_after):
        optimizer = torch.optim.SGD(model.parameters(), lr=0.002)
        loss      = nn.MSELoss()
        
        query  = torch.tensor([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],   dtype=torch.float32).view(2, -1, 4)
        key    = torch.tensor([3.0, 4.0, 5.0, 6.0, 1.0, 2.0, 3.0, 4.0],   dtype=torch.float32).view(2, -1, 4)
        value  = torch.tensor([5.0, 7.0, 8.0, 10.0, 10.0, 5.0, 2.0, 4.0], dtype=torch.float32).view(2, -1, 4)
        
        y = model.forward(query, key, value)
        self.assertTrue(torch.allclose(y, target_before, 1e-3, 1e-4))
        
        target = torch.tensor([[[ 5, -1,  2, 1]], [[ 4, -3, 1,  0.0]]], dtype=torch.float32)        
        l = loss(target, y)
        l.backward()
        
        optimizer.step();
        optimizer.zero_grad()

        query  = torch.tensor([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],   dtype=torch.float32).view(2, 1, 4)
        key    = torch.tensor([3.0, 4.0, 5.0, 6.0, 1.0, 2.0, 3.0, 4.0],   dtype=torch.float32).view(2, 1, 4)
        value  = torch.tensor([5.0, 7.0, 8.0, 10.0, 10.0, 5.0, 2.0, 4.0], dtype=torch.float32).view(2, 1, 4)
        
        y = model.forward(query, key, value)
        self.assertTrue(torch.allclose(y, target_after, 1e-3, 1e-4))
    
    def test_forward(self):
        torch.manual_seed(0)
        model         = MultiHeadedAttention(2, 4);    
        target_before = torch.tensor([[[ 4.9049, -0.8220,  1.5192,  1.1470]], [[ 3.6135, -2.5085,  0.6901,  0.0220]]], dtype=torch.float32)        
        target_after  = torch.tensor([[[ 4.9615, -0.8701,  1.5674,  1.1257]], [[ 3.6610, -2.5490,  0.7260,  0.0074]]], dtype=torch.float32)        
        self.test_model(model, target_before, target_after)
        
    def test_learn(self):
        torch.manual_seed(0)
        model         = MultiHeadedAttention(2, 4, 0.0);
        target_before = torch.tensor([[[ 4.4342, -0.7098, 1.3334, 1.0105]], [[3.2719, -2.2277, 0.5872, -0.0020]]], dtype=torch.float32)        
        target_after  = torch.tensor([[[ 4.5476, -0.7763, 1.3964, 1.0042]], [[3.3598, -2.2827, 0.6334, -0.0072]]], dtype=torch.float32)        
                
        torch.manual_seed(5)
        self.test_model(model, target_before, target_after)
                
    def test_clone(self):
        torch.manual_seed(0)
        model = MultiHeadedAttention(2, 4, 0.0);
        model_clone = copy.deepcopy(model)

        target_before = torch.tensor([[[ 4.4342, -0.7098, 1.3334, 1.0105]], [[3.2719, -2.2277, 0.5872, -0.0020]]], dtype=torch.float32)        
        target_after  = torch.tensor([[[ 4.5476, -0.7763, 1.3964, 1.0042]], [[3.3598, -2.2827, 0.6334, -0.0072]]], dtype=torch.float32)        
                
        torch.manual_seed(5)
        self.test_model(model, target_before, target_after)

        torch.manual_seed(5)
        self.test_model(model_clone, target_before, target_after)
        
    def test_forward_with_mask(self):
        torch.manual_seed(0)
        model     = MultiHeadedAttention(2, 4);        
        mask      = subsequent_mask(2)
        
        query  = torch.tensor([1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],   dtype=torch.float32).view(1, 2, -1)
        key    = torch.tensor([3.0, 4.0, 5.0, 6.0, 1.0, 2.0, 3.0, 4.0],   dtype=torch.float32).view(1, 2, -1)
        value  = torch.tensor([5.0, 7.0, 8.0, 10.0, 10.0, 5.0, 2.0, 4.0], dtype=torch.float32).view(1, 2, -1)
        
        y = model.forward(query, key, value, mask)
        
        target_after = torch.tensor([[[ 4.9049, -0.8220,  1.5192,  1.1470],[ 4.8924, -0.8384,  1.5108,  1.1367]]], dtype=torch.float32)               
        self.assertTrue(torch.allclose(y, target_after, 1e-3, 1e-4))
        
        
               
def multiheaded_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestMultiHeadedAttention('test_forward'))
    suite.addTest(TestMultiHeadedAttention('test_learn'))    
    suite.addTest(TestMultiHeadedAttention('test_clone'))
    suite.addTest(TestMultiHeadedAttention('test_forward_with_mask'))
    return suite
        