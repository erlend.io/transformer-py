import torch
import torch.nn as nn
import unittest
from tf.multiheadedattention import MultiHeadedAttention
from tf.positionwisefeedforward import PositionwiseFeedForward
from tf.encoderlayer import EncoderLayer
from tf.utils import subsequent_mask

class TestEncoderLayer(unittest.TestCase):

    def setUp(self):
        h       = 2      # number of heads                
        d_model = 4      # number of features                
        dropout = 0.1    # Dropout persentage
        d_ff    = 8      # Inner dimension of FeedForward network        
        
        torch.manual_seed(0)
        self.attn  = MultiHeadedAttention(h, d_model, dropout)
        torch.manual_seed(0)
        self.ff    = PositionwiseFeedForward(d_model, d_ff, dropout)
        self.model = EncoderLayer(d_model, self.attn, self.ff, dropout)
                
    def test_forward(self):
        # Dimension [Batch, Word, DWord]
        x    = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask = subsequent_mask(3)
        
        # Run model  
        log = None
        # log.print_model_parameters("name", self.model)
                    
        torch.manual_seed(0)
        y = self.model.forward(x, mask)
        
        if log is not None:            
            log.print_tensor("y", y)        
            
        target = torch.tensor([[[ 1.5759,  2.6246,  2.9671,  4.0485],[ 4.5868,  5.7981,  6.1672,  7.2702], [ 7.4528,  8.8088,  9.1085, 10.4069]]], dtype=torch.float32)
        self.assertTrue(torch.allclose(target, y, 1e-3, 1e-4))
        
    def test_learning(self):
        torch.manual_seed(0)                
        
        # setup optimizer        
        optimizer = torch.optim.SGD(self.model.parameters(), lr=0.00001)
        loss      = nn.MSELoss()
        
        # setup test example
        x      = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask   = subsequent_mask(3)
        target = torch.tensor([[[ 4.0,  2.0,  3, 5], [ 10,  7.0, 7.0,  8.0], [12.0, 9.0, 10.0, 10]]], dtype=torch.float32)
        
        target_before = torch.tensor([[[ 1.5759,  2.6246,  2.9671,  4.0485],[ 4.5868,  5.7981,  6.1672,  7.2702], [ 7.4528,  8.8088,  9.1085, 10.4069]]], dtype=torch.float32)
        target_after  = torch.tensor([[[1.8836,  2.8967,  3.1448,  3.9604],[4.9027,  5.8834,  6.4495,  7.2402],[7.1390,  8.9409,  9.2548, 10.2248]]], dtype=torch.float32)
        
        # run model
        log = None
        
        y = self.model.forward(x, mask)
        if log is not None:
            log.print_tensor("y", y)
            
        self.assertTrue(torch.allclose(y, target_before, 1e-3, 1e-4))
        
        # train        
        l = loss(y, target)
        
        l.backward()        
        optimizer.step();
        optimizer.zero_grad()
        
        # test result of training        
        y = self.model.forward(x, mask)
        if log is not None:
            log.print_tensor("ya", y)
        
        l = loss(target, y)
        self.assertTrue(torch.allclose(y, target_after, 1e-3, 1e-4))
        
                       
def test_encoder_layer():
    suite = unittest.TestSuite()
    suite.addTest(TestEncoderLayer('test_forward'))
    suite.addTest(TestEncoderLayer('test_learning'))
    return suite
