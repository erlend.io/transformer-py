import unittest
import math
import torch
import torch.nn as nn
from tf.embeddings import Embeddings

class TestEmbeddings(unittest.TestCase):
        
    def test_forward(self):        
        torch.manual_seed(0)
        model     = Embeddings(2, 20);        
        optimizer = torch.optim.SGD(model.parameters(), lr=0.1)
        loss      = nn.MSELoss()
               
        query  = torch.tensor([1, 2, 3, 4],   dtype=torch.int32).view(2, -1)
        y      = model.forward(query)       
        answer = torch.tensor([[[-0.3544, -0.6136], [ 1.2003,  0.9786]], [[-0.4469, -2.9914], [ 0.4558, -1.7866]]], dtype=torch.float32)        

        self.assertTrue(torch.allclose(y, answer, 1e-4, 1e-5))
        
        target = torch.tensor([[[0.0, -0.5], [ 1.0,  1.0]], [[-1.0, -3.0], [ 1.0, -2.0]]], dtype=torch.float32)        
        l = loss(target, y)
        l.backward()
        
        optimizer.step()
        optimizer.zero_grad()

        y = model.forward(query)
        
        target = torch.tensor([[[-0.3367, -0.6079], [ 1.1902,  0.9797]], [[-0.4746, -2.9918], [ 0.4830, -1.7973]]], dtype=torch.float32)
        self.assertTrue(torch.allclose(y, target, 1e-3, 1e-4))
        
    def test_weight(self):
        weight = torch.zeros(20,6)
        
        for i in range(10):
            weight[i][4]    = 1 + i/10
            weight[i+10][5] = 1 + i/10
        
        model     = Embeddings(6, 20);        
        model.lut = nn.Embedding.from_pretrained(weight)
        
        query  = torch.tensor([0, 5, 10, 15],   dtype=torch.int32).view(2, -1)
        y      = model.forward(query) / math.sqrt(6) 
        
        
        target = torch.tensor([[[0.0000, 0.0000, 0.0000, 0.0000, 1.0000, 0.0000], [0.0000, 0.0000, 0.0000, 0.0000, 1.5000, 0.0000]], [[0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1.0000],[0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1.5000]]], dtype=torch.float32)
        self.assertTrue(torch.allclose(y, target))
               
def test_embeddings():
    suite = unittest.TestSuite()
    suite.addTest(TestEmbeddings('test_forward'))
    suite.addTest(TestEmbeddings('test_weight'))
    return suite
