import unittest

import torch

from tf.makemodel import make_model
from tf.utils import subsequent_mask

def inference_test(seed):
    print("Stringing...")
    
    torch.manual_seed(seed)
    test_model = make_model(11, 11, 2, seed=seed)
    test_model.eval()
       
    src = torch.LongTensor([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]])
    src_mask = torch.ones(1, 1, 10)
    
    torch.manual_seed(seed*71+27)
    memory = test_model.encode(src, src_mask)
    ys = torch.zeros(1, 1).type_as(src)
    
    for i in range(9):
        out = test_model.decode(
            memory, src_mask, ys, subsequent_mask(ys.size(1)).type_as(src.data)
        )
#        log.print("out", out)
        
        prob = test_model.generator(out[:, -1])
        _, next_word = torch.max(prob, dim=1)
        next_word = next_word.data[0]
        ys = torch.cat(
            [ys, torch.empty(1, 1).type_as(src.data).fill_(next_word)], dim=1
        )
        
    return ys

def run_tests():
    for i in range(10):
        inference_test(i)

class TestInference(unittest.TestCase):
        
    def test(self):
        self.assertTrue(torch.allclose(inference_test(6), torch.LongTensor([[0, 10,  5, 10, 5, 10, 5, 8, 0, 8]])))
        self.assertTrue(torch.allclose(inference_test(7), torch.LongTensor([[0,  6,  8,  6, 6,  6, 4, 6, 4, 6]])))
        self.assertTrue(torch.allclose(inference_test(8), torch.LongTensor([[0,  4, 10,  2, 4,  4, 0, 7, 4, 4]])))
            
def inference_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestInference('test'))
    return suite
        