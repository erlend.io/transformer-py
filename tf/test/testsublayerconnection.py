import unittest
import torch
import torch.nn as nn
from tf.sublayerconnection import SublayerConnection


class TestSublayerConnection(unittest.TestCase):

    def sublayer_test(self, x):
        return x*4;

    def test_forward(self):
        torch.manual_seed(0)
        model     = SublayerConnection(3, 0.1);        
        
        # Dimension [Batch, Word, DWord]
        x = torch.tensor([[[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]]], dtype=torch.float32)        
        y = model.forward(x, lambda xp: self.sublayer_test(xp))
        target = torch.tensor([[[ 1.0000,  2.0000,  7.4444], [4.0000,  5.0000, 10.4444], [ 2.5556,  8.0000, 13.4444]]], dtype=torch.float32)
        self.assertTrue(torch.allclose(target, y, 1e-3, 1e-4))
        
    def test_learning(self):
        torch.manual_seed(0)
        model     = SublayerConnection(3, 0.1);        
        optimizer = torch.optim.SGD(model.parameters(), lr=0.001)
        loss      = nn.MSELoss()
        
        # Dimension [Batch, Word, DWord]
        x = torch.tensor([[[1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [7.0, 8.0, 9.0]]], dtype=torch.float32)        
        y = model.forward(x, lambda xp: self.sublayer_test(xp))       
        target = torch.tensor([[[ 1.0000,  2.0000,  8], [7.0000,  6.0000, 14], [ 3,  8.0000, 15]]], dtype=torch.float32)
        l = loss(target, y)
        l.backward()
        
        optimizer.step();
        optimizer.zero_grad()

        y = model.forward(x, lambda xp: self.sublayer_test(xp))       

        learning_target = torch.tensor([[[-3.4405,  2.0044,  7.4942],[-0.4405,  5.0044, 10.4942], [ 2.5595,  8.0044, 13.4942]]], dtype=torch.float32)
        self.assertTrue(torch.allclose(y, learning_target, 1e-3, 1e-4))
                       
def test_sublayer_connection():
    suite = unittest.TestSuite()
    suite.addTest(TestSublayerConnection('test_forward'))
    suite.addTest(TestSublayerConnection('test_learning'))
    return suite
        