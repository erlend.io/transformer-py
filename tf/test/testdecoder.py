import copy
import torch
import torch.nn as nn
import unittest
from tf.decoder import Decoder
from tf.decoderlayer import DecoderLayer
from tf.multiheadedattention import MultiHeadedAttention
from tf.positionalencoding import PositionalEncoding
from tf.positionwisefeedforward import PositionwiseFeedForward
from tf.utils import subsequent_mask

class TestDecoder(unittest.TestCase):

    def setUp(self):
        h         = 2      # number of heads                
        d_model   = 4      # number of features                
        dropout   = 0.1    # Dropout persentage
        d_ff      = 8      # Inner dimension of FeedForward network        
        
        torch.manual_seed(0)
        self.self_attn = MultiHeadedAttention(h, d_model, dropout)
        attn           = copy.deepcopy(self.self_attn)
        torch.manual_seed(0);
        self.ff    = PositionwiseFeedForward(d_model, d_ff, dropout)
        position   = PositionalEncoding(d_model, dropout)
        layer      = DecoderLayer(d_model, self.self_attn, attn, self.ff, dropout)
        self.model = Decoder(layer, 5)
        
    def test_forward(self):
        # Dimension [Batch, Word, DWord]
        target_before = torch.tensor([[[1.4574, -0.7777, -0.4816, -0.1980],[1.4690, -0.7620, -0.4294, -0.2776],[1.2467, -1.2019, -0.0117, -0.0331]]], dtype=torch.float32)
        
        x      = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mem    = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask   = subsequent_mask(3)
        
        # Run model  
        torch.manual_seed(0)
        y      = self.model.forward(x, mem, mask, mask)
        
        self.assertTrue(torch.allclose(target_before, y, 1e-3, 1e-4))
        
    def test_learning(self):
        torch.manual_seed(0)        

        # setup optimizer        
        optimizer = torch.optim.SGD(self.model.parameters(), lr=0.00001)
        loss      = nn.MSELoss()
        
        # setup test example
        x      = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mem    = torch.tensor([[[1.0, 2.0, 3.0, 4.0], [4.0, 5.0, 6.0, 7.0], [7.0, 8.0, 9.0, 10.0]]], dtype=torch.float32)        
        mask   = subsequent_mask(3)
        target = torch.tensor([[[4.0, 2.0, 3.0, 5.0], [10.0,7.0, 7.0, 8.0], [12.0, 9.0, 10.0, 10.0]]], dtype=torch.float32)
        
        target_before = torch.tensor([[[1.4574, -0.7777, -0.4816, -0.1980],[1.4690, -0.7620, -0.4294, -0.2776],[1.2467, -1.2019, -0.0117, -0.0331]]], dtype=torch.float32)
        target_after  = torch.tensor([[[1.4923, -0.6095, -0.5185, -0.3643],[1.4245, -0.9155, -0.2151, -0.2938],[1.3332, -1.0904, -0.1665, -0.0763]]], dtype=torch.float32)
        
        # run model
        y = self.model.forward(x, mem, mask, mask)
        self.assertTrue(torch.allclose(y, target_before, 1e-3, 1e-4))
        
        # train        
        l = loss(y, target)
        
        l.backward()        
        optimizer.step();
        optimizer.zero_grad()
        
        # test result of training        
        y = self.model.forward(x, mem, mask, mask)
        
        l = loss(target, y)
        self.assertTrue(torch.allclose(y, target_after, 1e-3, 1e-4))
        
                       
def test_decoder():
    suite = unittest.TestSuite()
    suite.addTest(TestDecoder('test_forward'))
    suite.addTest(TestDecoder('test_learning'))
    return suite
