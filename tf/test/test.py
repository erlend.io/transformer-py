import unittest
from tf.test.testbatch import batch_suite
from tf.test.testdecoder import test_decoder
from tf.test.testembeddings import test_embeddings
from tf.test.testfeedforward import test_feedforward
from tf.test.testgenerator import generator_suite
from tf.test.testlabelsmoothing import label_smoothing_suite
from tf.test.testmask import tast_mask_suite
from tf.test.testmultiheadedattention import multiheaded_suite
from tf.test.testpositionalencoding import test_positional_suite
from tf.test.testrufflyequal import ruffly_suite
from tf.test.testscheduler import scheduler_suite
from tf.test.testsimplelosscompute import simple_loss_compute
from tf.test.testattention import attention_suite
from tf.test.testencoder import test_encoder
from tf.test.testlayernorm import layer_norm_suite
from tf.test.testsublayerconnection import test_sublayer_connection
from tf.test.testencoderlayer import test_encoder_layer
from tf.test.testdecoderlayer import test_decoder_layer
from tf.test.testinference import inference_suite

runner = unittest.TextTestRunner()
runner.run(test_embeddings())
runner.run(test_encoder())
runner.run(ruffly_suite())
runner.run(simple_loss_compute())
runner.run(generator_suite())
runner.run(attention_suite())
runner.run(label_smoothing_suite())
runner.run(scheduler_suite())
runner.run(layer_norm_suite())
runner.run(test_sublayer_connection())
runner.run(test_encoder_layer())        
runner.run(test_decoder())        
runner.run(test_decoder_layer())      
runner.run(tast_mask_suite())  
runner.run(multiheaded_suite())
runner.run(test_feedforward())
runner.run(test_positional_suite())
runner.run(inference_suite())
runner.run(batch_suite())