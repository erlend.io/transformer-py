import unittest
import torch

from tf.positionalencoding import PositionalEncoding


class TestPositionalEncoding(unittest.TestCase):
        
    def test_forward(self):
        torch.manual_seed(0)
        pe = PositionalEncoding(6, 0, 200)
        y = pe.forward(torch.zeros(1, 6, 6))
        
        target = torch.tensor([[[ 0.0000,  1.0000,  0.0000,  1.0000,  0.0000,  1.0000],
         [ 0.8415,  0.5403,  0.0464,  0.9989,  0.0022,  1.0000],
         [ 0.9093, -0.4161,  0.0927,  0.9957,  0.0043,  1.0000],
         [ 0.1411, -0.9900,  0.1388,  0.9903,  0.0065,  1.0000],
         [-0.7568, -0.6536,  0.1846,  0.9828,  0.0086,  1.0000],
         [-0.9589,  0.2837,  0.2300,  0.9732,  0.0108,  0.9999]]], dtype=torch.float32)
        
        self.assertTrue(torch.allclose(y, target, 1e-5, 1e-4))
               
def test_positional_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestPositionalEncoding('test_forward'))
    return suite
        