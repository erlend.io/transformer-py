import torch
import unittest
from tf.labelsmoothing import LabelSmoothing

class TestLabelSmoothing(unittest.TestCase):
        
    def test(self):
        crit = LabelSmoothing(5, 0, 0.1)
        predict = torch.FloatTensor(
            [
                [0.1, 0.2, 0.7, 0.1, 0],
                [0.1, 0.2, 0.7, 0.1, 0],
                [0.1, 0.2, 0.7, 0.1, 0],
                [0.1, 0.2, 0.7, 0.1, 0],
                [0.1, 0.2, 0.7, 0.1, 0],
            ]
        )
        predict += 1e-8        
        predict_log = predict.log()
        y = crit(x=predict_log, target=torch.LongTensor([2, 1, 0, 3, 3]))
            
def label_smoothing_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestLabelSmoothing('test'))
    return suite
