import torch
import unittest
from tf.layernorm import LayerNorm

class TestLayerNorm(unittest.TestCase):
    def setUp(self):
        torch.manual_seed(0)
        self.norm = LayerNorm(4, 0.1)
        
    def test_forward(self):
        x = torch.tensor([1.0, 2.0, 3.0, 4.0], dtype=torch.float32)
        target = torch.tensor([-1.0784, -0.3595,  0.3595,  1.0784], dtype=torch.float32)
        y      = self.norm.forward(x)
        self.assertTrue(torch.allclose(target, y, 1e-3, 1e-5))
        
    def test_gradient(self) :
        x      = torch.tensor([1.0, 2.0, 3.0, 4.0], dtype=torch.float32)
        y      = self.norm.forward(x)
        target = torch.tensor([-1, 0.0,  0.0,  1], dtype=torch.float32)

        loss   = ((target - y)**2).sum()
        loss.backward()

        with torch.no_grad():
            self.norm.a_2 -= 0.01 * self.norm.a_2.grad
            self.norm.b_2 -= 0.01 * self.norm.b_2.grad        

        target_a2 = torch.tensor([0.1690, 0.2584, 0.2584, 0.1690], dtype=torch.float32)
        target_b2 = torch.tensor([-0.1567, -0.7189,  0.7189,  0.1567], dtype=torch.float32)        
        self.assertTrue(torch.allclose(self.norm.a_2.grad, target_a2, 1e-3, 1e-4))
        self.assertTrue(torch.allclose(self.norm.b_2.grad, target_b2, 1e-3, 1e-4))

        y = self.norm.forward(x)
        target = torch.tensor([-1.0750, -0.3513,  0.3513,  1.0750], dtype=torch.float32)
        self.assertTrue(torch.allclose(y, target, 1e-3, 1e-4))
        
        
def layer_norm_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestLayerNorm('test_forward'))
    suite.addTest(TestLayerNorm('test_gradient'))
    return suite
        