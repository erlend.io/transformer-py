import unittest
import torch
from tf.rufflyequal import ruffly_equal

class TestRufflyEqual(unittest.TestCase):
    def test_float(self):
        self.assertTrue(ruffly_equal(1.0, 1.0000001))
        self.assertFalse(ruffly_equal(1.0, 2.0))
        
    def test_diffent_types(self) :
        x = torch.tensor([5.5, 3], requires_grad=True)
        self.assertFalse(ruffly_equal(x, 5.5))
        
    def test_diffent_sizes(self) :
        v1 = torch.tensor([[1, 2, 3], [4, 5, 6]],   dtype=torch.float32)
        v2 = torch.tensor([[1, 2], [3, 4], [5, 6]], dtype=torch.float32)
        self.assertFalse(ruffly_equal(v1, v2))

    def test_diffent_types2(self) :
        v1 = torch.tensor([[1, 2, 3], [4, 5, 6]],   dtype=torch.float32)
        v2 = torch.tensor([[1, 2, 3], [4, 5, 6]],   dtype=torch.float64)
        self.assertFalse(ruffly_equal(v1, v2))

    def test_only_supports_floats(self) :
        v1 = torch.tensor([[1, 2, 3], [4, 5, 6]],   dtype=torch.float64)
        v2 = torch.tensor([[1, 2, 3], [4, 5, 6]],   dtype=torch.float64)
        self.assertFalse(ruffly_equal(v1, v2))

    def test_verify_content_different(self) :
        v1 = torch.tensor([[1, 2, 3], [4, 5, 6]],   dtype=torch.float32)
        v2 = torch.tensor([[1, 2, 3], [4, 5, 7]],   dtype=torch.float32)
        self.assertFalse(ruffly_equal(v1, v2))
        
    def test_verify_content_equal(self) :
        v1 = torch.tensor([[1, 2, 3], [4, 5.000001, 6]], dtype=torch.float32)
        v2 = torch.tensor([[1, 2.00001, 3], [4, 5, 6]],  dtype=torch.float32)
        self.assertTrue(ruffly_equal(v1, v2))

def ruffly_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestRufflyEqual('test_float'))
    suite.addTest(TestRufflyEqual('test_diffent_types'))
    suite.addTest(TestRufflyEqual('test_diffent_sizes'))
    suite.addTest(TestRufflyEqual('test_diffent_types2'))
    suite.addTest(TestRufflyEqual('test_only_supports_floats'))
    suite.addTest(TestRufflyEqual('test_verify_content_different'))
    suite.addTest(TestRufflyEqual('test_verify_content_equal'))    
    return suite
