import torch

def loss(x, crit):
    d = x + 3 * 1
    predict = torch.FloatTensor([[0.0, x / d, 1 / d, 1 / d, 1 / d]])
    predict = predict + 1e-8    
    return crit(predict.log(), torch.LongTensor([1])).data
