import sys

class Log:
    def __init__(self):
        self.out = sys.__stdout__

    def enter(self, name) :
        self.println("----- " + name + " -----")

    def endl(self) :
        print(file=self.out)

    def exit(self) :
        self.println("--------------")

    def print(self, text) :
        print(text, end="", file=self.out)

    def println(self, text) :
        print(text, file=self.out)

    def print_float(self, name, value):
        self.println(name + ": " + str(value))

    def print_int(self, name, integer):
        self.println(name + ": " + str(integer))

    def print_tensor(self, name, tensor) :
        self.println("--- " + name + " ---")
        self.print_tensor_dim(tensor, 0),
        self.endl()        
        self.print_shape(tensor)
        if tensor.requires_grad == True :
            self.println("Req: True")
        self.println("--------------")

    def print_tensor_dim(self, tensor, current_dim) :
        if tensor.dim() == 0:
            self.print_scalar(tensor)
        elif tensor.dim() == 1:
            if (tensor.size(0) < 8) :
                self.print_vector(tensor, 0, tensor.size(0))
            else:
                self.print("[")
                self.print_vector(tensor, 0, 3),
                self.print(", ...,")
                self.print_vector(tensor, tensor.size(0)-3, tensor.size(0))
                self.print("]")
        else:
            count = tensor.size(0);

            self.print("[")
            if (count < 8) :
                self.print_section(tensor, 0, count, current_dim);
            else:
                self.print_section(tensor, 0, 3, current_dim);
                self.println(",")
                self.tab(1 + current_dim);
                self.println("...,")
                self.tab(1 + current_dim);
                self.print_section(tensor, count -3, count, current_dim);

            self.print("]")

    def print_model_parameters(self, name, model) :
        self.println("----- " + name + " parameters -----")
        for name, param in model.named_parameters():
            self.print_tensor(name, param)
        self.println("-----------------------------")

    def print_vector(self, tensor, start, stop):
        for i in range(start, stop):
            if i > start:
                self.print(", ")
            self.print_scalar(tensor[i])

    def print_scalar(self, tensor):
        self.print(f"{tensor.item():7.4f}".format())

    def print_section(self, tensor, start, stop, current_dim):
        for i in range(start, stop) :
            if i > start :
                self.println(",")
                if tensor.dim() > 2 :
                    self.endl()

                self.tab(1 + current_dim);

            self.print_tensor_dim(tensor[i], current_dim + 1);

    def print_shape(self, tensor):
        self.print("Shape: [")
        for i in range(tensor.dim()) :
            if i > 0 :
                self.print(", ")
            self.print(tensor.size(i))
        self.print("]")
        self.endl()        

    def tab(self, spaceCount) :
        for i in range(spaceCount) :
            self.print(" ")




class FLog(Log):

    def __init__(self, filename):
        self.out = open(filename, "w")

    def close(self):
        self.out.close()

