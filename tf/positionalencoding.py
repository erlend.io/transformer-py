import math
import torch.nn as nn
import torch

class PositionalEncoding(nn.Module):
    "Implement the PE function."
    
    def __init__(self, d_model, dropout, max_len=10000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        # Compute the positional encodings once in log space.
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, d_model, 2) * -(math.log(10000.0) / d_model)
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        
        pe2 = self.calc(d_model, max_len)              
        pe = pe.unsqueeze(0)
        pe2 = pe2.unsqueeze(0)
                        
        self.register_buffer("pe", pe)
        
    def calc(self, d_model, max_len):
        pe2 = torch.zeros(max_len, d_model)
        
        for pos in range(max_len):
            for i in range(0, d_model//2, 1):
                div             = 10000 ** (2*i / d_model)
                pe2[pos, 2*i]   = math.sin(pos / div)
                pe2[pos, 2*i+1] = math.cos(pos / div)
    
        return pe2;

    def forward(self, x):
        x = x + self.pe[:, : x.size(1)].requires_grad_(False)
        return self.dropout(x)
        