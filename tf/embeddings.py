import torch.nn as nn
import math

class Embeddings(nn.Module):
    def __init__(self, d_model, vocab):
        super(Embeddings, self).__init__()
        self.lut = nn.Embedding(vocab, d_model)
        self.d_model = d_model
        self.log     = None        

    def forward(self, x):
        if self.log :
            self.log.enter("EmbeddingsImpl::forward");
            self.log.print_tensor("x", x);
            lu = self.lut(x);        
            self.log.print_tensor("lut(x)", lu);
            y = lu * math.sqrt(self.d_model);
            self.log.print_tensor("y", lu * math.sqrt(self.d_model));
            self.log.exit();                    
        else: 
            y = self.lut(x) * math.sqrt(self.d_model)

        return y

    def set_log(self, log):
        self.log = log        
  