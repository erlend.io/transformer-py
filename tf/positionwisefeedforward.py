import torch.nn as nn

class PositionwiseFeedForward(nn.Module):
    "Implements FFN equation."

    def __init__(self, d_model, d_ff, dropout=0.1):
        # [Batch, Word, d_model]
        super(PositionwiseFeedForward, self).__init__()
        self.log = None
        self.w_1 = nn.Linear(d_model, d_ff)
        self.w_2 = nn.Linear(d_ff, d_model)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        y = self.w_2(self.dropout(self.w_1(x).relu()))
        if self.log is not None:
            self.log.enter("PositionwiseFeedForwardImpl::forward");        
            self.log.print_tensor("x", x);
            self.log.print_tensor("y", y);
            self.log.exit();        
        
        return y
    
    def setLog(self, log):
        self.log = log
